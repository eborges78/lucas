<?php
/**
 * EventsController.php
 *
 * @since   22/12/2017
 * @copyright Copyright (c) Windataco 2016 All Rights Reserved
 */

declare(strict_types=1);

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EventsController extends Controller
{
    use DasTrait;

    /**
     * @Route("/{sportName}/{competitionName}-{id}/", name="events",
     *     requirements={"sportName"="[\w-]+", "competitionName"="[\w-]+", "id"="[\d+]"})
     * @param string $sportName
     * @param string $competitionName
     * @return
     */
    public function index(string $sportName, string $competitionName)
    {
        $events = [];
        for ($i=0; $i < rand(3, 15); $i++) {
            $events[] = 'equipe '.rand(1, 100). ' - equipe '.rand(1, 100);
        }
        $params = [
            'sports' => $this->getSportList(),
            'competitions' => [
                'football' => $this->getCompetitionBySportList('football'),
                'tennis' => $this->getCompetitionBySportList('tennis'),
                'rugby' => $this->getCompetitionBySportList('rugby'),
                'basketball' => $this->getCompetitionBySportList('basketball')
            ],
            'events' => $events,
            'sportName' => $sportName,
            'competitionName' => str_replace('-', ' ', $competitionName)
        ];

        dump($params);
        return $this->render('resultats.html.twig', $params);
    }
}