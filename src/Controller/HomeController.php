<?php
/**
 * HomeController.php
 *
 * @since   22/12/2017
 * @copyright Copyright (c) Windataco 2016 All Rights Reserved
 */

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    use DasTrait;
    /**
     * @Route("/", name="home")
     */
    public function indexAction()
    {
        $params = [
            'sports' => $this->getSportList()
        ];
        dump($params);
        return $this->render('accueil.html.twig', $params);
    }
}
