<?php
/**
 * SportController.php
 *
 * @since   22/12/2017
 * @copyright Copyright (c) Windataco 2016 All Rights Reserved
 */

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class SportController extends Controller
{
    use DasTrait;
    /**
     * @Route("/{sportName}/", name="sport", requirements={"sportName"="[\w-]+"})
     */
    public function index(string $sportName)
    {
        $params = [
            'sports' => $this->getSportList(),
            'competitions' => [
                'football' => $this->getCompetitionBySportList('football'),
                'tennis' => $this->getCompetitionBySportList('tennis'),
                'rugby' => $this->getCompetitionBySportList('rugby'),
                'basketball' => $this->getCompetitionBySportList('basketball')
            ],
            'sportName' => $sportName
        ];

        dump($params);
        return $this->render('sport.html.twig', $params);
    }
}
