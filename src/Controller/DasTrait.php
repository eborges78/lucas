<?php
/**
 * DasTrait.php
 *
 * @since   22/12/2017
 * @copyright Copyright (c) Windataco 2016 All Rights Reserved
 */

declare(strict_types=1);

namespace App\Controller;

trait DasTrait
{
    public function getSportList()
    {
        return [
            'football',
            'tennis',
            'rugby',
            'basketball'
        ];
    }

    public function getCompetitionBySportList(string $sport)
    {
        if ($sport === 'football') {
            return [
                'Ligue 1',
                'Liga',
                'Jupiler league',
                'serie A'
            ];
        }

        if ($sport === 'tennis') {
            return [
                'coupe davis',
                'roland garros',
                'wimbledon'
            ];
        }

        if ($sport === 'rugby') {
            return [
                'Top 14',
                'Pro D2'
                
            ];
        }

        if ($sport === 'basketball') {
            return [
                'NBA',
                'Pro A',
				'ACB'
            ];
        }
        return [];
    }
}
