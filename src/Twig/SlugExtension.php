<?php
/**
 * SlugExtension.php
 *
 * @since   22/12/2017
 * @copyright Copyright (c) Windataco 2016 All Rights Reserved
 */

declare(strict_types=1);

namespace App\Twig;

class SlugExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('slug', [$this, 'slug']),
        ];
    }
    public function slug(string $text)
    {
        $text = trim(strtolower($text));
        $text = str_replace(' ', '-', $text);
        return $text;
    }
}